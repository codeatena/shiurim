//
//  Category.h
//  Shiurim
//
//  Created by Shani on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject

@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *cateDesc;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *subCateId;
@property (nonatomic, strong) NSString *subCateName;

-(id)initWithDictionary:(NSDictionary*)_data;

@end
