//
//  DownloadManager.m
//  MultiDownloadManager
//
//  Created by Ahsan on 4/8/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//
#import "Download.h"

NSString *const kProgressChanged = @"ProgressChanged";

#import "DownloadManager.h"

static DownloadManager *instance = nil;

@implementation DownloadManager

+ (DownloadManager *)instance{
 
    if (instance == nil) {
        
        instance = [[DownloadManager alloc] init];
    }
    
    return instance;
    
}
- (id)init
{
    self = [super init];
    
    if (self) {
        
        service = [BaseService new];
        downloads = [NSMutableDictionary new];
    }
    
    return self;
}
- (void) downloadFileFromUrl:(NSString*)fileURL
                 witFileName:(NSString*)fileName
                      ofType:(NSString *)extension
                        ofSong:(Song *)_song
{
    
    self.isDownloading=YES;
    [service downloadFileFromUrl:fileURL witFileName:fileName ofType:extension andSong:_song andDelegate:self];
}

#pragma mark service delegates
- (void) service:(BaseService*)service reponseRecieved:(id)object andStatusInfo:(NSDictionary *)statusInfo{
    
   
    Song *song = [(NSArray *)object lastObject];
    
    NSLog(@"---%@_____",song.songid);
    
    NSLog(@"statusInfo=%@ and Response=%@",statusInfo,object);
    UIAlertView *alert = [[UIAlertView alloc] init];
	[alert setTitle:@"Message"];
    [alert setMessage:@"DOWNLOAD COMPLETE"];
	[alert setDelegate:self];
	[alert addButtonWithTitle:@"Ok"];
	[alert show];
    

//    self.isDownloaded=YES;
}
- (void) service:(BaseService*)service failedWithStatusInfo:(NSDictionary *)statusInfo{

    NSLog(@"%@",statusInfo);
}

- (void) service:(BaseService *)service progress:(CGFloat)progress forFile:(NSString *)fileURL{
    
    NSDictionary *info = @{@"url":fileURL ,@"progress":[NSNumber numberWithFloat:progress]};
    [[NSNotificationCenter defaultCenter] postNotificationName:kProgressChanged object:self userInfo:info];
    
//    NSLog(@"[%@]",fileURL);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Download Complete" object:self userInfo:nil];

            break;
        default:
            break;
    }
}
@end
