//
//  BaseService.h
//  Shiurim
//
//  Created by Shani on 4/6/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Song.h"

// Success and Failure Blocks
typedef id (^SuccessBlock)(NSDictionary *);
typedef id (^FailBlock) (NSDictionary *);

// SuccessAnd Failure Response Blocks
typedef void (^ResponseLoadedBlock)(id,NSDictionary*);
typedef void (^ErrorBlock)(NSDictionary*);

typedef enum{
    
    RequestTypeGetCats,
    RequestTypeGetSubCats,
    RequestTypeGetSongs,
    RequestTypeAddFavorite,
    RequestTypeGetFavorite,
    RequestTypeDownloadFile
        
}RequestType;

// Request Method
typedef enum{
    
    RequestMethodPost,
    RequestMethodGet,
    RequestMethodPut,
    RequestMethodDelete
    
}RequestMethod;

@protocol ServiceDelegate;

@interface BaseService : NSObject

@property (nonatomic, weak) id<ServiceDelegate> delegate;

- (void) executeRequestType:(RequestType)_type
              requestMethod:(RequestMethod)_method
                        URL:(NSString*)_urlAction
                 parameters:(NSMutableDictionary *)_params
               successBlock:(SuccessBlock)_success
               failureBlock:(FailBlock)_fail;

- (void) downloadFileFromUrl:(NSString*)_catalogueUrl
                 witFileName:(NSString*)_name
                      ofType:(NSString *)_extension
                     andSong:(Song *)_song
                 andDelegate:(id<ServiceDelegate>)_delegate;

- (NSDictionary*) statusInfoWithResponse:(NSArray*)response andRequestType:(RequestType)requestType;
- (void) performSuccessAction:(NSDictionary *)_data;
- (void) performFailAction:(NSDictionary *)_data;

//These functions are used to notify if in case we're getting response from some other API e.g open weather or youtube
- (void)successResponse: (NSArray*)data withRequestType: (RequestType)requestType;
- (void)failureResponse: (NSError*)error withRequestType: (RequestType)requestType;

@end

@protocol ServiceDelegate <NSObject>
@optional
- (void) service:(BaseService*)service reponseRecieved:(id)object andStatusInfo:(NSDictionary *)statusInfo;
- (void) service:(BaseService*)service failedWithStatusInfo:(NSDictionary *)statusInfo;
- (void) service:(BaseService *)service progress:(CGFloat)progress forFile:(NSString *)fileURL;
- (void) service:(BaseService *)service progress:(CGFloat)progress;

@end
