//
//  SongsListViewController.h
//  Shiurim
//
//  Created by Ahsan on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "BaseViewController.h"
#import "XmlService.h"

@interface SongsListViewController : BaseViewController{

    IBOutlet UITableView *songsTable;
    NSMutableArray *songs;
    XmlService *xmlService;
}
@property (nonatomic, retain) NSString *subCatId;

@end
