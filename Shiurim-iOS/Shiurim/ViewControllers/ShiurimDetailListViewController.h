//
//  ShiurimDetailViewController.h
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "XmlService.h"
#import "Category.h"

@interface ShiurimDetailListViewController : BaseViewController<UISearchBarDelegate>{
    
    
    XmlService *xmlService;
    
    NSMutableArray *categories;
    NSMutableArray *searchResults;
    BOOL isSearching;
}
@property (strong, retain) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property (strong, nonatomic) Category *parentCategory;

@end
