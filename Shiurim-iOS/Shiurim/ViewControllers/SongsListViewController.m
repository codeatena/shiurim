//
//  SongsListViewController.m
//  Shiurim
//
//  Created by Ahsan on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "SongsListViewController.h"
#import "Song.h"
#import "ShiurimDetailViewController.h"
#import "AppDelegate.h"

@interface SongsListViewController ()

@end

@implementation SongsListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    songs = [NSMutableArray new];
    xmlService = [XmlService new];
   // [xmlService getSongsWithDelegate:self];
    [xmlService getSongsofSubCategory:self.subCatId WithDelegate:self];
    [self showProgressForView:self.view WithMessage:@"Loading..."];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = UIColorFromRGB(0xf7f7f7);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark TalbeView Delegate & DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 46.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [songs count];;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
//        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellbk"]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

    }
    
    Song *song=[songs objectAtIndex:indexPath.row];
    cell.textLabel.text=song.songName;
    cell.detailTextLabel.text=song.date;
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"ShiurimDetail" sender:indexPath];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"ShiurimDetail"]) {
        
        ShiurimDetailViewController *detailVC= (ShiurimDetailViewController *)[segue destinationViewController];
        NSIndexPath *indexPath = (NSIndexPath*)sender;
        Song *song = [songs objectAtIndex:indexPath.row];
        [detailVC setSelectedSong:song];
    }
}
#pragma mark - ServiceDelegate
- (void) service:(BaseService*)service reponseRecieved:(id)object andStatusInfo:(NSDictionary *)statusInfo{
    
    [super service:service reponseRecieved:object andStatusInfo:statusInfo];
    
    NSArray *result = (NSArray *)object;
    [songs addObjectsFromArray:result];
    [songsTable reloadData];
    
}
-(void)service:(BaseService *)service failedWithStatusInfo:(NSDictionary *)statusInfo{
    [super service:service failedWithStatusInfo:statusInfo];
    NSLog(@"Fail msg %@",statusInfo);
}

@end
