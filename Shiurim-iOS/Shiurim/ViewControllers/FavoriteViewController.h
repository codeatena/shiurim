//
//  FavoriteViewController.h
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface FavoriteViewController : BaseViewController{

    UIBarButtonItem *editBtn;
    BOOL isEditing;
    NSMutableArray *favorites;
    NSMutableArray *searchResults;
    BOOL isSearching;
}
@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end
