//
//  DownloadViewController.m
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "DownloadViewController.h"
#import "AppDelegate.h"
#import "Download.h"
#import "ShiurimDetailViewController.h"

@interface DownloadViewController ()

@end

@implementation DownloadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    downloads =[NSMutableArray new];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [downloads addObjectsFromArray:[Download getDownloadsInManagedObjectContext:appDelegate.managedObjectContext]];
    [listTableView reloadData];
    
    self.view.backgroundColor = UIColorFromRGB(0xf7f7f7);
}

- (void)editAction{
    
    isEditing=!isEditing;
    if(isEditing){
        
        editBtn.title=@"Done";
        [listTableView setEditing:YES];
        
    }
    else{
        
        editBtn.title=@"Edit";
        [listTableView setEditing:NO];
    }
    
}
- (void)addRightBtn{
    
    editBtn = [[UIBarButtonItem alloc]
               initWithTitle:@"Edit"
               style:UIBarButtonItemStylePlain
               target:self
               action:@selector(editAction)];
    [self.navigationItem setRightBarButtonItem:editBtn];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"Downloads";
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self addRightBtn];
    self.automaticallyAdjustsScrollViewInsets = NO;
    listTableView.contentInset = UIEdgeInsetsZero;
}
#pragma mark TalbeView Delegate & DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 46.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [downloads count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
   
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    Download *song=(Download *)[downloads objectAtIndex:indexPath.row];
    NSString *songTxt=@"";
    if([song.downloaded intValue]== 1){
    
        songTxt=[NSString stringWithFormat:@"%@    (In progress)",song.songName];
    }else
        songTxt=song.songName;
    
    cell.textLabel.text =songTxt;
    
    [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
    cell.detailTextLabel.text=@"Size: 3.56 MB";
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
//    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellbk"]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Download *song=(Download *)[downloads objectAtIndex:indexPath.row];
    
    if ([song.downloaded intValue] == 1) {
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Message"];
        [alert setMessage:@"Download in progress"];
        [alert setDelegate:self];
        [alert addButtonWithTitle:@"Ok"];
        [alert show];
        
    }else{
        [self performSegueWithIdentifier:@"ShiurimDetail" sender:indexPath];
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete){
        
        Download *download=[downloads objectAtIndex:indexPath.row];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.managedObjectContext deleteObject:download];
        [appDelegate saveContext];
        [downloads removeObjectAtIndex:indexPath.row];
        [listTableView reloadData];
    }
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ShiurimDetailViewController *ctr=[segue destinationViewController];
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    Download  *song = [downloads objectAtIndex:indexPath.row];
    
    Song *selectedSong=[[Song alloc] init];
    selectedSong.songName=song.songName;
    selectedSong.songid=song.songId;
    selectedSong.link=song.songURL;
    [ctr setSelectedSong:selectedSong];
    [ctr setIsFavorite:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
