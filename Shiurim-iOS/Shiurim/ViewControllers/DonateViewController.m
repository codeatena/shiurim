//
//  DonateViewController.m
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "DonateViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

// dev
//#define kPayPalClientId @"AWw_FhB9jkl8pBg1EVkSEE1zMorVPMAiUKA91iWYQxuYPpCZnjJHu1lTA4vA"

// live
#define kPayPalClientId @"AcP-wRDWmq7zHH9uPQZE_cbfkOjkL-qMK9ldMS2Jfj1ic8LRrJIo2H0LZmqz"

#define kPayPalReceiverEmail @"dovidg224@gmail.com"


@interface DonateViewController ()

@end

@implementation DonateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
//    [PayPalPaymentViewController setEnvironment:PayPalEnvironmentNoNetwork];
//    [PayPalPaymentViewController prepareForPaymentUsingClientId:kPayPalClientId];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"Donate";

    self.view.backgroundColor = UIColorFromRGB(0xf7f7f7);
}


#pragma mark - PayPalPaymentDelegate methods

- (void)verifyCompletedPayment:(PayPalPayment *)completedPayment {
    // Send the entire confirmation dictionary
    NSData *confirmation = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation
                                                           options:0
                                                             error:nil];
    
    // Send confirmation to your server; your server should verify the proof of payment
    // and give the user their goods or services. If the server is not reachable, save
    // the confirmation and try again later.
}

- (void)payPalPaymentDidComplete:(PayPalPayment *)completedPayment {
    // Payment was processed successfully; send to server for verification and fulfillment.
    [self verifyCompletedPayment:completedPayment];
    
    // Dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel {
    // The payment was canceled; dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)payPalAction:(id)sender {
    
    // Create a PayPalPayment
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = [[NSDecimalNumber alloc] initWithString:@"15.00"];
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Awesome songs";
    
    // Check whether payment is processable.
    if (!payment.processable) {
        // If, for example, the amount was negative or the shortDescription was empty, then
        // this payment would not be processable. You would want to handle that here.
    }
    NSString *customerId = @"user-11723590050";
    
    
//    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithClientId:kPayPalClientId
//                                                                                                 receiverEmail:kPayPalReceiverEmail
//                                                                                                       payerId:customerId
//                                                                                                       payment:payment
//                                                                                                      delegate:self];
//    PayPalPaymentViewController *paymentViewController = [PayPalPaymentViewController alloc] initW
//    [self presentViewController:paymentViewController animated:YES completion:nil];

}
@end
