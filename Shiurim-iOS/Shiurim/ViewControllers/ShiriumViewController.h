//
//  ShiriumViewController.h
//  Shiurim
//
//  Created by Shani on 3/30/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "XmlService.h"

@interface ShiriumViewController : BaseViewController{

    
    BOOL isEditing;
    NSMutableArray *categories;
    NSMutableArray *searchResults;
    BOOL isSearching;
    XmlService *xmlService; // test commit

}
@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end
