//
//  ShiurimDetailViewController.m
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "ShiurimDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "Favorite.h"
#import "Download.h"
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVAudioSession.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVPlayerItem.h>
#import <MediaPlayer/MediaPlayer.h>
#import "DownloadManager.h"
#import "Utility.h"

@interface ShiurimDetailViewController ()

@end

@implementation ShiurimDetailViewController

@synthesize progDownload;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    
    
    [super viewDidLoad];
    xmlService = [XmlService new];
    isPlaying=NO;
    fav=NO;
    if(self.isFavorite){
    
        [self.downloadBtn setHidden:YES];
        [self.favoriteBtn setHidden:YES];
        [self.progDownload setHidden:YES];
    }
    self.title=self.selectedSong.songName;
    self.downloadBtn.layer.cornerRadius=6.0;
    self.favoriteBtn.layer.cornerRadius=6.0;
    titleLabel.text=self.selectedSong.songName;
    
    lblStartTime.text = @"00:00";
    lblEndTime.text = @"-00:00";
    [sldPlay setValue: 0.0f animated:YES];
    progDownload.hidden = YES;
    sldPlay.hidden = NO;
    
    UIImage* image = [UIImage imageNamed:@"slider.png"];
    image = [AppDelegate imageWithImage:image scaledToSize:CGSizeMake(2, 20)];
    [sldPlay setThumbImage:image forState:UIControlStateNormal];
    
    self.view.backgroundColor = UIColorFromRGB(0xf7f7f7);
}
- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(progressChange:) name:kProgressChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadComplete:) name:@"Download Complete" object:nil];
//    if([[DownloadManager instance] isDownloading]){
//    
//        [self.downloadBtn setEnabled:NO];
//    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    NSArray *viewControllers = self.navigationController.viewControllers;
    if (viewControllers.count > 1 && [viewControllers objectAtIndex:viewControllers.count-2] == self) {
        // View is disappearing because a new view controller was pushed onto the stack
        NSLog(@"New view controller was pushed");
    } else if ([viewControllers indexOfObject:self] == NSNotFound) {
        // View is disappearing because it was popped from the stack
        NSLog(@"View controller was popped");
        if ([audioPlayer isPlaying]) {
            [audioPlayer stop];
            audioPlayer = nil;
        }
    }
}

- (void) didReceiveMemoryWarning
{

}

- (void) progressChange:(NSNotification *)ntf{

    NSLog(@"[__%@__]",ntf);
    CGFloat progressVal=[[[ntf userInfo] objectForKey:@"progress"] floatValue];
    [self.progDownload setProgress:progressVal];
}

- (void) downloadComplete:(NSNotification*) ntf {
    NSLog(@"Download Complete");
    sldPlay.hidden = NO;
    self.progDownload.hidden = YES;
    [self.progDownload setProgress:0];
}

- (void)showConfirmAlert
{
    
	UIAlertView *alert = [[UIAlertView alloc] init];
	[alert setTitle:@"Confirm"];
	[alert setMessage:@"Are You Sure You Want To Add as a Favorite ?"];
	[alert setDelegate:self];
	[alert addButtonWithTitle:@"Yes"];
	[alert addButtonWithTitle:@"No"];
	[alert show];
}

- (IBAction)playAction:(id)sender {
    
    NSURL *url=nil;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([Download isAlreadyDownloaded:self.selectedSong inManagedObjectContext:appDelegate.managedObjectContext]){
    
        NSString* path=[Utility cacheDirectoryPathWithFileName:self.selectedSong.songName  ofType:@"mp3"];
        NSLog(@"path is %@",path);
        url = [NSURL fileURLWithPath:path];
    }
    else{
        url = [NSURL URLWithString:[self.selectedSong.link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"%@__URL___%@",[self.selectedSong.link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],url);
        
    }
    
    if (audioPlayer == nil) {
        NSData *songData=[NSData dataWithContentsOfURL:url];
        
        audioPlayer = [[AVAudioPlayer alloc] initWithData:songData error:nil];
        
        NSLog(@"duration = %f", audioPlayer.duration);
        
        lblEndTime.text = [NSString stringWithFormat:@"-%@", [self changeTimetoFloat:audioPlayer.duration]];
        
        [sldPlay setMaximumValue: audioPlayer.duration];
    }

    if(moviePlayerController ==nil){
        
//        moviePlayerController =  [[MPMoviePlayerController alloc] initWithContentURL:url];
        
//        moviePlayerController.useApplicationAudioSession = YES;
//        [[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(moviePlaybackComplete:)
//												 name:MPMoviePlayerPlaybackDidFinishNotification
//											   object:moviePlayerController];
  

        

        //        NSLog(@"duration = %f", moviePlayerController.duration);

//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(MPMoviePlayerLoadStateDidChange:)
//                                                     name:MPMoviePlayerLoadStateDidChangeNotification
//                                                   object:nil];
    }
    
    if(!isPlaying) {
//        NSLog(@"movieplayer duration = %f", moviePlayerController.duration);

//        [moviePlayerController play];
        playTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updatePlayTime) userInfo:nil repeats:YES];
        
//        audioPlayer.numberOfLoops = 0;
        audioPlayer.delegate=self;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        [audioPlayer prepareToPlay];
        [audioPlayer play];
        
        [self.playBtn setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        isPlaying=!isPlaying;

    }else{
        [audioPlayer pause];
//        [moviePlayerController pause];
              [self.playBtn setBackgroundImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        isPlaying=!isPlaying;
    }
}

- (void) updatePlayTime
{
    
//    NSLog(@"current time = %f", audioPlayer.currentTime);
    NSString* strPlayedTime = [self changeTimetoFloat: audioPlayer.currentTime];
    
    float fRemainTime = audioPlayer.duration - audioPlayer.currentTime;
    NSString* strRemainTime = [NSString stringWithFormat: @"-%@", [self changeTimetoFloat: fRemainTime]];
    
    [sldPlay setValue: audioPlayer.currentTime animated:YES];
    [lblStartTime setText: strPlayedTime];
    [lblEndTime setText: strRemainTime];
}

- (IBAction) onPlaySlider: (id) sender
{
    [audioPlayer setCurrentTime: sldPlay.value];
}

- (NSString*) changeTimetoFloat: (float) length
{
    int minutes = (int) floor(length / 60);
    int seconds = (int) length - (minutes * 60);
    
    NSString* strMinutes = [NSString stringWithFormat: @"%d", minutes];
    if (minutes < 10) {
        strMinutes = [NSString stringWithFormat: @"0%@", strMinutes];
    }
    
    NSString* strSeconds = [NSString stringWithFormat: @"%d", seconds];
    if (seconds < 10) {
        strSeconds = [NSString stringWithFormat: @"0%@", strSeconds];
    }
    
    NSString *strTime = [NSString stringWithFormat: @"%@:%@", strMinutes, strSeconds];
    
    return strTime;
}

- (void)MPMoviePlayerLoadStateDidChange:(NSNotification *)notification
{
    if ((moviePlayerController.loadState & MPMovieLoadStatePlaythroughOK) == MPMovieLoadStatePlaythroughOK)
    {
        NSLog(@"content play length is %g seconds", moviePlayerController.duration);
    }
}

- (IBAction)downloadAction:(id)sender {


    
    fav=NO;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    int songstate = [Download songStatus:self.selectedSong inManagedObjectContext:appDelegate.managedObjectContext];
    
    NSString *message = @"";
    switch (songstate) {
        case 0:{
            sldPlay.hidden = YES;
            progDownload.hidden = NO;
            [Download addDownload:self.selectedSong inManagedObjectContext:appDelegate.managedObjectContext];
            [[DownloadManager instance] downloadFileFromUrl:self.selectedSong.link  witFileName:_selectedSong.songName ofType:@"mp3" ofSong:self.selectedSong];
            
            [appDelegate saveContext];
            return;
        }
            break;
        case 1:
            message = @"Download in progress";
            break;
        case 2:
            message = @"Already downloaded";
            break;
            
        default:
            break;
    }

    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Message"];
    [alert setMessage:message];
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"Ok"];
    [alert show];

//    sldPlay.hidden = NO;
//    self.progressView.hidden = YES;
    
//    if([Download isAlreadyDownloaded:self.selectedSong inManagedObjectContext:appDelegate.managedObjectContext]){
//    
//        UIAlertView *alert = [[UIAlertView alloc] init];
//        [alert setTitle:@"Message"];
//        [alert setMessage:@"Already downloaded"];
//        [alert setDelegate:self];
//        [alert addButtonWithTitle:@"Ok"];
//        [alert show];
//        
//    }else{
//    
//        
//        [Download addDownload:self.selectedSong inManagedObjectContext:appDelegate.managedObjectContext];
//        [[DownloadManager instance] downloadFileFromUrl:self.selectedSong.link  witFileName:_selectedSong.songName ofType:@"mp3" ofSong:self.selectedSong];
//
//        [[DownloadManager instance] setIsDownloading:YES];
//        [self.downloadBtn setEnabled:NO];
//        [appDelegate saveContext];
//        
//      
//    }
    
//    [xmlService downloadFileFromUrl:_selectedSong.link witFileName:_selectedSong.songName ofType:@"mp3" andDelegate:self];
    
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [playTimer invalidate];
    playTimer = nil;
    lblStartTime.text = @"00:00";
    lblEndTime.text = [NSString stringWithFormat:@"-%@", [self changeTimetoFloat:audioPlayer.duration]];

    [sldPlay setValue:0 animated:YES];
    isPlaying = NO;
    [self.playBtn setBackgroundImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}

- (IBAction)favoriteAction:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([Favorite isAlreadyFavorite:self.selectedSong inManagedObjectContext:appDelegate.managedObjectContext]){
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Message"];
        [alert setMessage:@"Already Favorite"];
        [alert setDelegate:self];
        [alert addButtonWithTitle:@"Ok"];
        [alert show];
        
    }else{

    
        fav=YES;
  
        [self showConfirmAlert];
    }
    
}

- (void) service:(BaseService *)service progress:(CGFloat)progress{
    
    [self.progDownload setProgress:progress];
}

#pragma mark AlertDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    switch (buttonIndex) {
        case 0:{
            
            if(fav){
         
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [Favorite addFavorite:self.selectedSong inManagedObjectContext:appDelegate.managedObjectContext];
                [appDelegate saveContext];
                fav=NO;
            }
        }
            break;
            
        default:
            break;
    }
}
@end
