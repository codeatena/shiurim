//
//  DonateViewController.h
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PayPalMobile.h"

@interface DonateViewController : BaseViewController<PayPalPaymentDelegate>
- (IBAction)payPalAction:(id)sender;

@end
