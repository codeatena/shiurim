//
//  ShiurimDetailViewController.m
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "ShiurimDetailListViewController.h"
#import "Category.h"
#import "AFNetworking.h"
#import "SongsListViewController.h"
#import "AppDelegate.h"

@interface ShiurimDetailListViewController ()

@end

@implementation ShiurimDetailListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    categories=[NSMutableArray new];
    searchResults=[NSMutableArray new];
    isSearching=NO;

    xmlService = [[XmlService alloc] init];
  //  [xmlService getSubCategoriesWithDelegate:self];
    [xmlService getSubCategoriesOfCategory:self.parentCategory.categoryId WithDelegate:self];
    [self showProgressForView:self.view WithMessage:@"Loading..."];
    [self.listTableView setTableHeaderView:self.searchBar];
    
    self.view.backgroundColor = UIColorFromRGB(0xf7f7f7);

 //   self.title=@"Shiurim";
}

- (void) songRequestWithWihtSubcatId:(NSString *)catid{
    
    NSString *url  = [NSString stringWithFormat:@" http://aceweboptimization.com/iphone/grossman2/songsreq.php?subcatid=%@",catid];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:url]];
    
    
    NSMutableURLRequest *req = [client requestWithMethod:@"GET" path:url parameters:nil];
    
    AFHTTPRequestOperation *call = [client HTTPRequestOperationWithRequest:req success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        NSLog(@"__%@___",operation.responseString);
        
   
    [self performSegueWithIdentifier:@"SongsDetail" sender:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
    }];
    
    [call start];
}
#pragma mark TalbeView Delegate & DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 46.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(isSearching)
        return [searchResults count];
    else
        return [categories count];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if(isSearching){
        
        Category *cat=[searchResults objectAtIndex:indexPath.row];
        cell.textLabel.text=cat.subCateName;
    }
    else{
        
        Category *cat=[categories objectAtIndex:indexPath.row];
        cell.textLabel.text =cat.subCateName;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellbk"]];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
    
    
    return cell;

    
    
    
    /*
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    if(isSearching){
        
        Category *cat=[searchResults objectAtIndex:indexPath.row];
        cell.textLabel.text=cat.subCateName;
    }
    else{
  
        Category *cat=[categories objectAtIndex:indexPath.row];
    
        cell.textLabel.text=cat.subCateName;
    }
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];

    
    [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellbk"]];
    
    return cell;
     
     */
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    Category *cat=[categories objectAtIndex:indexPath.row];
//    [self songRequestWithWihtSubcatId:cat.subCateId];
    
    [self performSegueWithIdentifier:@"SongsDetail" sender:indexPath];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    SongsListViewController *ctr=[segue destinationViewController];
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    Category *cat=[categories objectAtIndex:indexPath.row];
    [ctr setSubCatId:cat.subCateId];
    [ctr.navigationItem setTitle:cat.subCateName];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ServiceDelegate
- (void) service:(BaseService*)service reponseRecieved:(id)object andStatusInfo:(NSDictionary *)statusInfo{
    
    [super service:service reponseRecieved:object andStatusInfo:statusInfo];
    
    NSArray *result = (NSArray *)object;
    [categories addObjectsFromArray:result];
    
    [self.listTableView reloadData];
    
}
-(void)service:(BaseService *)service failedWithStatusInfo:(NSDictionary *)statusInfo{
    [super service:service failedWithStatusInfo:statusInfo];
    
    NSLog(@"Fail msg %@",statusInfo);
}
- (void) startSearchChanges{
    
    [self.searchBar setShowsCancelButton:YES animated:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.listTableView.allowsSelection = NO;
    self.listTableView.scrollEnabled = NO;
    
}
- (void) endSearchChanges{
    
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.listTableView.allowsSelection = YES;
    self.listTableView.scrollEnabled = YES;
    
}
#pragma mark searchbar delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"subCateName contains[cd] %@",
                                    self.searchBar.text];
    [searchResults removeAllObjects];
    isSearching=YES;
    [searchResults addObjectsFromArray:[categories filteredArrayUsingPredicate:resultPredicate]];
    if(self.searchBar.text.length>0)
        isSearching=YES;
    else
        isSearching=NO;
    
    [self.listTableView reloadData];

    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    [self endSearchChanges];
    [self.searchBar resignFirstResponder];
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"subCateName contains[cd] %@",
                                    self.searchBar.text];
    [searchResults removeAllObjects];
    isSearching=YES;
    [searchResults addObjectsFromArray:[categories filteredArrayUsingPredicate:resultPredicate]];
    if(self.searchBar.text.length>0)
        isSearching=YES;
    else
        isSearching=NO;
    
    [self.listTableView reloadData];
    
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    [self startSearchChanges];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    
    [self endSearchChanges];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar
{
    [self endSearchChanges];
    [self.searchBar resignFirstResponder];
    if(self.searchBar.text.length>0)
        isSearching=YES;
    else
        isSearching=NO;
    
    [self.listTableView reloadData];
}

@end
