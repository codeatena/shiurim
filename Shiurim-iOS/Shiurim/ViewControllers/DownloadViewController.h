//
//  DownloadViewController.h
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface DownloadViewController : BaseViewController{

    IBOutlet UITableView *listTableView;
    NSMutableArray *downloads;
    UIBarButtonItem *editBtn;
    BOOL isEditing;    
    
}

@end
