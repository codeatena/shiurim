//
//  FavoriteViewController.m
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//
#import "ShiurimDetailViewController.h"
#import "FavoriteViewController.h"
#import "AppDelegate.h"
#import "Favorite.h"
#import "Song.h"

@interface FavoriteViewController ()

@end

@implementation FavoriteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)editAction{
    
    isEditing=!isEditing;
    if(isEditing){
        
        editBtn.title=@"Done";
        [self.listTableView setEditing:YES];
        
    }
    else{
        
        editBtn.title=@"Edit";
        [self.listTableView setEditing:NO];
    }
    
}
- (void)addRightBtn{
    
    editBtn = [[UIBarButtonItem alloc]
               initWithTitle:@"Edit"
               style:UIBarButtonItemStylePlain
               target:self
               action:@selector(editAction)];
    [self.navigationItem setRightBarButtonItem:editBtn];
}
- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    favorites=[NSMutableArray new];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [favorites addObjectsFromArray:[Favorite getFavoritesInManagedObjectContext:appDelegate.managedObjectContext]];
    [self.listTableView reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isEditing=NO;
    isSearching=NO;
    searchResults=[NSMutableArray new];
    self.title=@"Favorites";
    [self.listTableView setTableHeaderView:self.searchBar];
   //    for(int i=0;i<10;i++){
//    
//        [favorites addObject:[NSString stringWithFormat:@"SongMp3 %d",i+1]];
//        
//    }
    
    [self.listTableView setBackgroundView:nil];
    [self.listTableView setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self addRightBtn];
    
    self.view.backgroundColor = UIColorFromRGB(0xf7f7f7);

}

- (void) startSearchChanges{
    
    [self.searchBar setShowsCancelButton:YES animated:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.listTableView.allowsSelection = NO;
    self.listTableView.scrollEnabled = NO;

    
}
- (void) endSearchChanges{
    
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.listTableView.allowsSelection = YES;
    self.listTableView.scrollEnabled = YES;
    
    
}
#pragma mark searchbar delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"songName contains[cd] %@",
                                    self.searchBar.text];
    [searchResults removeAllObjects];
    isSearching=YES;
    
    NSArray *searched =[favorites filteredArrayUsingPredicate:resultPredicate];
    [searchResults addObjectsFromArray:searched];
    
    if(self.searchBar.text.length>0)
        isSearching=YES;
    else
        isSearching=NO;
    
    [self.listTableView reloadData];

    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    [self endSearchChanges];
    [self.searchBar resignFirstResponder];
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    self.searchBar.text];
    [searchResults removeAllObjects];
    isSearching=YES;
    [searchResults addObjectsFromArray:[favorites filteredArrayUsingPredicate:resultPredicate]];
    if(self.searchBar.text.length>0)
        isSearching=YES;
    else
        isSearching=NO;
    
    [self.listTableView reloadData];

}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    [self startSearchChanges];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    
    [self endSearchChanges];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar
{
    [self endSearchChanges];
    [self.searchBar resignFirstResponder];
    if(self.searchBar.text.length>0)
        isSearching=YES;
    else
        isSearching=NO;
    
    [self.listTableView reloadData];

    
}
#pragma mark TalbeView Delegate & DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 46.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(isSearching)
        return [searchResults count];
    else
        return [favorites count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    if(isSearching){
        
        Favorite *fav=[searchResults objectAtIndex:indexPath.row];
        
        cell.textLabel.text =fav.songName;

    }
    else{
        
        Favorite *fav=[favorites objectAtIndex:indexPath.row];
        
        cell.textLabel.text =fav.songName;
    }
    [cell.textLabel setFont:[UIFont systemFontOfSize:15.0]];
    cell.detailTextLabel.text=@"Size: 3.56 MB";
//    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellbk"]];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"ShiurimDetail" sender:indexPath];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ShiurimDetailViewController *ctr=[segue destinationViewController];
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    Favorite *fav;
    if(isSearching)
        fav=   [searchResults objectAtIndex:indexPath.row];
    else
        fav = [favorites objectAtIndex:indexPath.row];
    
    Song *selectedSong=[[Song alloc] init];
    selectedSong.songName=fav.songName;
    selectedSong.songid=fav.songId;
    selectedSong.link=fav.songURL;
    [ctr setSelectedSong:selectedSong];
    [ctr setIsFavorite:YES];
    
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete){
    
         Favorite *fav=[favorites objectAtIndex:indexPath.row];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.managedObjectContext deleteObject:fav];
        [appDelegate saveContext];
        [favorites removeObjectAtIndex:indexPath.row];
        [self.listTableView reloadData];
    }

}
#pragma mark SearchBar Delegate Methods


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
