//
//  ShiurimDetailViewController.h
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Song.h"
#import "XmlService.h"
#import "BaseViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface ShiurimDetailViewController : BaseViewController<AVAudioPlayerDelegate>{

    IBOutlet UILabel *titleLabel;
    
    XmlService *xmlService;
    MPMoviePlayerController *moviePlayerController;
    BOOL isPlaying;
    BOOL fav;
    
    IBOutlet UISlider* sldPlay;
    
    IBOutlet UILabel* lblStartTime;
    IBOutlet UILabel* lblEndTime;
    
    NSTimer* playTimer;
    AVAudioPlayer *audioPlayer;
}

@property (strong, nonatomic) IBOutlet UIButton *playBtn;

@property (strong, nonatomic) IBOutlet UIButton *favoriteBtn;
@property (strong, nonatomic) IBOutlet UIProgressView *progDownload;
@property (assign, nonatomic   ) BOOL isFavorite;
@property (nonatomic, strong) Song *selectedSong;

- (IBAction)playAction:(id)sender;
- (IBAction)downloadAction:(id)sender;
- (IBAction)favoriteAction:(id)sender;

- (IBAction) onPlaySlider: (id) sender;

@property (strong, nonatomic) IBOutlet UIButton *downloadBtn;

@end
