//
//  ContactViewController.m
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "ContactViewController.h"
#import "AppDelegate.h"

@interface ContactViewController ()

@end

@implementation ContactViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"Contact Us";
    
    self.view.backgroundColor = UIColorFromRGB(0xf7f7f7);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callEvent:(id)sender {
    
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {

        NSString *phoneNo=@"tel:3234222009";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNo]];
        
    }else{
    
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Message"];
        [alert setMessage:@"Your device doesn't support this feature."];
        [alert setDelegate:self];
        [alert addButtonWithTitle:@"Ok"];
        [alert show];
    }
}
@end
