//
//  DownloadManager.h
//  MultiDownloadManager
//
//  Created by Ahsan on 4/8/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

extern NSString *const kProgressChanged;

#import <Foundation/Foundation.h>
#import "BaseService.h"
#import "Song.h"

@interface DownloadManager : BaseService<ServiceDelegate>{
    
    BaseService *service;
    NSMutableDictionary *downloads;
}

+ (DownloadManager *) instance;

@property (nonatomic, assign) BOOL isDownloaded;
@property (nonatomic, assign) BOOL isDownloading;

- (void) downloadFileFromUrl:(NSString*)fileURL
                 witFileName:(NSString*)fileName
                      ofType:(NSString *)extension
                    ofSong:(Song *)_song  ;
@end
