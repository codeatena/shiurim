//
//  XmlService.m
//  Shiurim
//
//  Created by Shani on 4/6/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "XmlService.h"
#import "AFHTTPRequestOperation.h"
#import "XMLDictionary.h"
#import "Constants.h"
#import "BaseService.h"
#import "Category.h"
#import "Song.h"

@implementation XmlService
- (void)failureResponse:(NSError *)error {
    // Deal with failure
    NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    
    NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetCats];
    
    NSMutableDictionary *responseDict = [NSMutableDictionary dictionary];
    [responseDict setObject:error forKey:ERROR];
    [responseDict setObject:requestType forKey:REQUEST_TYPE];
    [self performFailAction:[NSDictionary dictionaryWithDictionary:responseDict]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void) getCategoriesWithDelegate:(id<ServiceDelegate>)_delegate{

    self.delegate=_delegate;
    NSURL *url = [NSURL URLWithString:@"http://aceweboptimization.com/iphone/grossman2/xmls/cats.xml"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
      //  NSLog(@"Response is %@",operation.responseString);
            NSDictionary *xmlDict=[NSDictionary dictionaryWithXMLString:operation.responseString];
        NSLog(@"Xml dic is %@",xmlDict);
        
        NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetCats];
        NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
        NSMutableArray *cats=[NSMutableArray new];
        
        for (NSDictionary *dic in [xmlDict objectForKey:category]){
        
            Category *cat=[[Category alloc ] initWithDictionary:dic ];
            [cats addObject:cat];
        }
        
        NSDictionary *responseDict = @{DATA: cats,STATUS_INFO: statusDict};
        [self performSuccessAction:responseDict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
     
    }];
    
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        
    }];
    
    [operation start];

}


- (void) getSubCategoriesWithDelegate:(id<ServiceDelegate>)_delegate{
    
    self.delegate=_delegate;
    NSURL *url = [NSURL URLWithString:@"http://aceweboptimization.com/iphone/grossman2/xmls/subcats.xml"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //  NSLog(@"Response is %@",operation.responseString);
        NSDictionary *xmlDict=[NSDictionary dictionaryWithXMLString:operation.responseString];
        NSLog(@"Xml dic is %@",xmlDict);
        
        NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetCats];
        NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
        NSMutableArray *cats=[NSMutableArray new];
        
        if ([[xmlDict objectForKey:category] isKindOfClass:[NSArray class]]) {
            
        
            for (NSDictionary *dic in [xmlDict objectForKey:category]){
                
                Category *cat=[[Category alloc ] initWithDictionary:dic ];
                [cats addObject:cat];
            }
        }else{
            
            Category *cat=[[Category alloc ] initWithDictionary:[xmlDict objectForKey:category] ];
            [cats addObject:cat];

        }
        NSDictionary *responseDict = @{DATA: cats,STATUS_INFO: statusDict};
        [self performSuccessAction:responseDict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
    }];
    
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        
    }];
    
    [operation start];
    
}

- (void) getSubCategoriesOfCategory:(NSString *)_catId WithDelegate:(id<ServiceDelegate>)_delegate{

    NSString *urll  = [NSString stringWithFormat:@"http://aceweboptimization.com/iphone/grossman2/subcatreq.php?catid=%@",_catId];
    self.delegate=_delegate;
    NSURL *url = [NSURL URLWithString:urll];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    NSError *error = nil;
        
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        if([JSON isKindOfClass:[NSArray class]]){
        
            if([JSON count]<1){
                
                NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetSubCats];
                NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
                
                [self performFailAction:statusDict];

                
                return ;
            }
        }
        NSArray *keys=[JSON allKeys];
        NSMutableArray *cats=[NSMutableArray new];
        for (int i=0;i<[keys count];i++){
        
            NSDictionary *data=[JSON objectForKey:[keys objectAtIndex:i]];
            Category *cat=[[Category alloc ] initWithDictionary:data];
            [cats addObject:cat];
            NSLog(@"sub category name = %@", cat.subCateName);

        }
        NSLog(@"Response is %@",operation.responseString);
        NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetSubCats];
        NSDictionary *statusDict = @{REQUEST_TYPE: requestType};

        NSDictionary *responseDict = @{DATA: cats,STATUS_INFO: statusDict};
        [self performSuccessAction:responseDict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetSubCats];
        NSDictionary *statusDict = @{REQUEST_TYPE: requestType};

        [self performFailAction:statusDict];
        
    }];
    
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        
    }];
    
    [operation start];
    
}
- (void) getSongsofSubCategory:(NSString *)_subCatId WithDelegate:(id<ServiceDelegate>)_delegate{
    
    NSString *urll  = [NSString stringWithFormat:@"http://aceweboptimization.com/iphone/grossman2/songsreq.php?subcatid=%@",_subCatId];
    self.delegate=_delegate;
    NSURL *url = [NSURL URLWithString:urll];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
        if([JSON isKindOfClass:[NSArray class]]){
            
            if([JSON count]<1){
                
                NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetSubCats];
                NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
                
                [self performFailAction:statusDict];
                
                return ;
                
            }
        }else if([JSON isKindOfClass:[NSDictionary class]]){
            
            NSArray *data = [JSON objectForKey:@"1"];
            
            if (data ==nil || [data count]<1) {
                                
                NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetSubCats];
                NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
                
                [self performFailAction:statusDict];
                
                return ;
            }else{
                
                NSArray *keys=[JSON allKeys];
                NSMutableArray *songs=[NSMutableArray new];
                for (int i=0;i<[keys count];i++){
                    
                    NSDictionary *data=[JSON objectForKey:[keys objectAtIndex:i]];
                    Song *song=[[Song alloc ] initWithDictionary:data];
                    [songs addObject:song];
                }
                NSLog(@"Response is %@",operation.responseString);
                NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetSubCats];
                NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
                
                NSDictionary *responseDict = @{DATA: songs,STATUS_INFO: statusDict};
                [self performSuccessAction:responseDict];
                return;
            }
        }
        NSArray *keys=[JSON allKeys];
        NSMutableArray *songs=[NSMutableArray new];
        for (int i=0;i<[keys count];i++){
            
            NSDictionary *data=[JSON objectForKey:[keys objectAtIndex:i]];
            Song *song=[[Song alloc ] initWithDictionary:data];
            [songs addObject:song];
        }
        NSLog(@"Response is %@",operation.responseString);
        NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetSongs];
        NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
        
        NSDictionary *responseDict = @{DATA: songs,STATUS_INFO: statusDict};
        [self performSuccessAction:responseDict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetSubCats];
        NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
        
        [self performFailAction:statusDict];
        
    }];
    
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        
    }];
    
    [operation start];
    
}

- (void) getSongsWithDelegate:(id<ServiceDelegate>)_delegate{
    
    self.delegate=_delegate;
    NSURL *url = [NSURL URLWithString:@"http://aceweboptimization.com/iphone/grossman2/xmls/songs.xml"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //  NSLog(@"Response is %@",operation.responseString);
        NSDictionary *xmlDict=[NSDictionary dictionaryWithXMLString:operation.responseString];
        NSLog(@"Xml Songs %@",xmlDict);
        
        NSString *requestType = [NSString stringWithFormat:@"%d",RequestTypeGetCats];
        NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
        NSMutableArray *songs=[NSMutableArray new];
        
        if ([[xmlDict objectForKey:@"song"] isKindOfClass:[NSArray class]]) {
            
            
            for (NSDictionary *dic in [xmlDict objectForKey:@"song"]){
                
                Song *song=[[Song alloc ] initWithDictionary:dic ];
                [songs addObject:song];
            }
        }else{
            
            Song *song=[[Song alloc ] initWithDictionary:[xmlDict objectForKey:@"song"]];
            [songs addObject:song];
            
        }
        NSDictionary *responseDict = @{DATA: songs,STATUS_INFO: statusDict};
        
        [self performSuccessAction:responseDict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
    }];
    
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        
    }];
    
    [operation start];
    
}

@end
