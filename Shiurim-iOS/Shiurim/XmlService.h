//
//  XmlService.h
//  Shiurim
//
//  Created by Shani on 4/6/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "BaseService.h"

@interface XmlService : BaseService

- (void) getCategoriesWithDelegate:(id<ServiceDelegate>)_delegate;
- (void) getSubCategoriesWithDelegate:(id<ServiceDelegate>)_delegate;
- (void) getSongsWithDelegate:(id<ServiceDelegate>)_delegate;
- (void) getSubCategoriesOfCategory:(NSString *)_catId
                       WithDelegate:(id<ServiceDelegate>)_delegate;
- (void) getSongsofSubCategory:(NSString *)_subCatId
                  WithDelegate:(id<ServiceDelegate>)_delegate;

@end
