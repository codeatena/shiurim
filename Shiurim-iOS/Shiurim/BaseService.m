//
//  BaseService.m
//  Shiurim
//
//  Created by Shani on 4/6/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "AFHTTPClient.h"
#import "BaseService.h"
#import "AFJSONRequestOperation.h"
#import "Constants.h"
#import "Utility.h"
#import "Download.h"
#import "AppDelegate.h"

@implementation BaseService

@synthesize delegate;

- (BOOL) validdateServerResponse:(id)_response{
    //#warning Handle Properly for Valid Server Responses. VIVALARASSA. May Be a "return Yes;" can do it 4 u.
    
    if ([_response isKindOfClass:[NSArray class]] ||
        [_response isKindOfClass:[NSDictionary class]]){
        return YES;
    }
    
    BOOL  status = [[_response objectForKey:RESPONSE] boolValue];
    
    return (status == NO) ? NO : YES;
    
    NSDictionary *response = [_response objectForKey:RESPONSE];
    
    if (response == nil || [response isKindOfClass:[NSNull class]]) {
        return NO;
    }
    return YES;
    
}
// performs success action
- (void) performSuccessAction:(NSDictionary *)_data{
    
    if (self.delegate!=nil && [self.delegate respondsToSelector:@selector(service:reponseRecieved:andStatusInfo:)]) {
        
        [self.delegate service:self reponseRecieved:[_data objectForKey:DATA]  andStatusInfo:[_data objectForKey:STATUS_INFO]];
    }
}

// performs fail action
- (void) performFailAction:(NSDictionary *)_data{
    
    [self.delegate service:self failedWithStatusInfo:_data];
}

- (NSString *) requestMethodWithType:(RequestMethod)_method{
    
    switch (_method) {
            
        case RequestMethodPost: return @"POST"; break;
            
        case RequestMethodPut:  return @"PUT";  break;
            
        case RequestMethodDelete:return @"DELETE"; break;
            
        case RequestMethodGet:  return @"GET"; break;
            
        default:return @"GET";
    }
}
- (NSDictionary*) statusInfoWithResponse:(NSArray*)response andRequestType:(RequestType)type{
    
    NSDictionary *statusDict = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",type],REQUEST_TYPE, nil];
    NSDictionary *responseDict = [NSDictionary dictionaryWithObjectsAndKeys:response,DATA,statusDict,STATUS_INFO, nil];
    return responseDict;
}



#pragma mark -
//These functions are used to notify if in case we're getting response from some other API e.g open weather or youtube

- (void) successResponse:(NSArray *)data withRequestType:(RequestType)requestType{
    
    NSString *requestTypeStr = [NSString stringWithFormat:@"%d",requestType];
    
    NSDictionary *statusDict = [NSDictionary dictionaryWithObjectsAndKeys:requestTypeStr,REQUEST_TYPE, nil];
    
    NSDictionary *responseDict = [NSDictionary dictionaryWithObjectsAndKeys:data,DATA,statusDict,STATUS_INFO, nil];
    [self performSuccessAction:responseDict];
    
}

- (void)failureResponse:(NSError *)error withRequestType:(RequestType)requestType{
    
    NSString *requestTypeStr = [NSString stringWithFormat:@"%d",requestType];
    
    NSMutableDictionary *response = [NSMutableDictionary dictionary];
    response[ERROR] = error;
    response[REQUEST_TYPE] = requestTypeStr;
    response[RESPONSE] = response;
    
    [self performFailAction:[NSDictionary dictionaryWithDictionary:response]];
    
}



#pragma mark - download File

- (void) downloadFileFromUrl:(NSString*)fileURL
                 witFileName:(NSString*)_name
                      ofType:(NSString *)_extension
                        andSong:(Song *)_song
                 andDelegate:(id<ServiceDelegate>)_delegate{
    
    self.delegate = _delegate;
    RequestType _type = RequestTypeDownloadFile;
    fileURL=[fileURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:fileURL];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    NSString* path=[Utility cacheDirectoryPathWithFileName:_name ofType:_extension];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"SUCCCESSFULL File RETRIEVE to %@!",path);
        NSString *requestType = [NSString stringWithFormat:@"%d",_type];
        NSDictionary *statusDict = @{REQUEST_TYPE: requestType};
        
        NSArray *parsedData = @[_song];

        
        NSDictionary *responseDict = @{DATA: parsedData,STATUS_INFO: statusDict};
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [Download updateDownload:_song inManagedObjectContext:appDelegate.managedObjectContext];
        [appDelegate saveContext];
       // [Download addDownload:_song inManagedObjectContext:appDelegate.managedObjectContext];
        
        
        [self performSuccessAction:responseDict];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // Deal with failure
        NSLog(@"Un SUCCCESSFULL File RETRIEVE to %@!",path);
        NSString *requestType = [NSString stringWithFormat:@"%d",_type];
        
        NSMutableDictionary *responseDict = [NSMutableDictionary dictionary];
        responseDict[ERROR] = error;
        responseDict[REQUEST_TYPE] = requestType;
        responseDict[RESPONSE] = @[];
        
        [self performFailAction:[NSDictionary dictionaryWithDictionary:responseDict]];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
    

    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead){
        
        float progressValue = (float)totalBytesRead / (float)totalBytesExpectedToRead;
        
//        NSLog(@"%f",progressValue);
        
        if ([self.delegate respondsToSelector:@selector(service:progress:forFile:)]) {
            
            [self.delegate service:self progress:progressValue forFile:fileURL];
        }
        
        if ([self.delegate respondsToSelector:@selector(service:progress:)]) {
            
            [self.delegate service:self progress:progressValue];
        }

    }];
    
    [operation start];
}

@end
