//
//  Constants.h
//  Shiurim
//
//  Created by Shani on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#ifndef Shiurim_Constants_h
#define Shiurim_Constants_h

#define ERROR                   @"error"
#define MESSAGE                 @"message"
#define REQUEST_TYPE            @"request_type"
#define RESPONSE                @"response"
#define RESPONSE_MESSAGE        @"resposne_message"
#define DATA                    @"data"
#define STATUS_INFO             @"status_info"

//--------------Category Model Keys -----------

#define cat_id      @"categoryId"
#define cat_name    @"categoryName"
#define category    @"category"

//-----------------------------------------------
#endif
