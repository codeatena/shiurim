//
//  Utility.h
//  Spontify
//
//  Created by Ahsan Ali on 7/21/12.
//  Copyright (c) 2012 WhiteSpaceConflict. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+ (NSString *) documentDirectoryPathForFile:(NSString *)_fileName;
+ (NSString *) documentDirectoryPathWithFileName:(NSString *)_name ofType:(NSString *)_extension;
+ (NSString *) cacheDirectoryPathWithFileName:(NSString *)_name ofType:(NSString *)_extension;
@end
