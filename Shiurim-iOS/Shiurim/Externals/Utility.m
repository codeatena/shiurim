//
//  Utility.m
//  Spontify
//
//  Created by Ahsan Ali on 7/21/12.
//  Copyright (c) 2012 WhiteSpaceConflict. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+ (NSString *) cacheDirectoryPathWithFileName:(NSString *)_name ofType:(NSString *)_extension{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = paths[0];
    NSString *fileName = nil;
    
    if (_extension == nil) {
        
        fileName = [NSString stringWithFormat:@"%@",_name];
        
    }else {
        
        fileName = [NSString stringWithFormat:@"%@.%@",_name,_extension];
        
    }
    
    NSString *writableDBPath = [cacheDirectory stringByAppendingPathComponent:fileName];
    
    return writableDBPath;
    
}

//file name and extension
+ (NSString *) documentDirectoryPathWithFileName:(NSString *)_name ofType:(NSString *)_extension{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *fileName = nil;
    
    if (_extension == nil) {
        
        fileName = [NSString stringWithFormat:@"%@",_name];
        
    }else {
        
        fileName = [NSString stringWithFormat:@"%@.%@",_name,_extension];
        
    }
    
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    return writableDBPath;
    
}

// _fileName with extension
+ (NSString *) documentDirectoryPathForFile:(NSString *)_fileName{
    
    return [self documentDirectoryPathWithFileName:_fileName ofType:nil];
    
}

@end
