//
//  Favorite.m
//  Shiurim
//
//  Created by Ahsan on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "Favorite.h"
#import "Song.h"

@implementation Favorite

@dynamic songId;
@dynamic songName;
@dynamic songURL;
@dynamic catId;

+ (void ) addFavorite:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context{

    
    Favorite *favorite = (Favorite *)[NSEntityDescription insertNewObjectForEntityForName:@"Favorite" inManagedObjectContext:_context];
    [favorite setSongId:_song.songid];
    [favorite setSongName:_song.songName];
    [favorite setSongURL:_song.link];
    [favorite setCatId:_song.subCateId];
    
}
+ (NSArray *) getFavoritesInManagedObjectContext:(NSManagedObjectContext *)_context{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Favorite"
                                              inManagedObjectContext:_context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError *error;
    
    NSArray *results = [_context executeFetchRequest:request error:&error] ;
    
    return results;
    
}
+(BOOL) isAlreadyFavorite:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context{
    
    BOOL exists=NO;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Favorite"
                                              inManagedObjectContext:_context];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"songId = %@ ", _song.songid];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setPredicate:predicate];
    [request setEntity:entity];
    NSError *error;
    
    NSArray *results = [_context executeFetchRequest:request error:&error] ;
    
    if([results count]>0)
        exists=YES;
    
    return exists;
}

@end
