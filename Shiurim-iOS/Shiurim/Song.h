//
//  Song.h
//  Shiurim
//
//  Created by Ahsan on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Song : NSObject
-(id)initWithDictionary:(NSDictionary*)_data;


@property (nonatomic, strong) NSString * songid;
@property (nonatomic, strong) NSString * date;
@property (nonatomic, strong) NSString * songDesc;
@property (nonatomic, strong) NSString * link;
@property (nonatomic, strong) NSString * subCateId;
@property (nonatomic, strong) NSString * songName;

@end
