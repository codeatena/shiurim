//
//  Download.h
//  Shiurim
//
//  Created by Ahsan on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Song.h"

@interface Download : NSManagedObject

@property (nonatomic, retain) NSString * catId;
@property (nonatomic, retain) NSString * songId;
@property (nonatomic, retain) NSString * songName;
@property (nonatomic, retain) NSString * songURL;
@property (nonatomic, retain) NSNumber * downloaded;

+ (void ) addDownload:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context;
+ (NSArray *) getDownloadsInManagedObjectContext:(NSManagedObjectContext *)_context;
+(BOOL) isAlreadyDownloaded:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context;
+ (void) updateDownload:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context;
+(int) songStatus:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context;

@end
