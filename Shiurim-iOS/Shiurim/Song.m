//
//  Song.m
//  Shiurim
//
//  Created by Ahsan on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "Song.h"

@implementation Song


-(id)initWithDictionary:(NSDictionary*)_data{
    
    
    if(self=[super init]){
        
        self.songid=    [_data objectForKey:@"id"];
        self.date =[_data objectForKey:@"date"];
        self.songDesc=[_data objectForKey:@"description"];
        self.link=[_data objectForKey:@"link"];
        self.subCateId=[_data objectForKey:@"subcatid"];
        self.songName =[_data objectForKey:@"name"];
        
    }
    return self;
}
@end
