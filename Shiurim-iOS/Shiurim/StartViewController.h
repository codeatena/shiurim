//
//  StartViewController.h
//  Shiurim
//
//  Created by Ahsan on 4/20/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *splashImage;

@end
