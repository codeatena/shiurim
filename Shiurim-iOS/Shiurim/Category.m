//
//  Category.m
//  Shiurim
//
//  Created by Shani on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "Category.h"
#import "Constants.h"

@implementation Category

-(id)initWithDictionary:(NSDictionary*)_data{

    
    if(self=[super init]){
        
        self.categoryId=    [_data objectForKey:cat_id];
        self.categoryName=  [_data objectForKey:cat_name];
        self.date =[_data objectForKey:@"date"];
        self.cateDesc=[_data objectForKey:@"description"];
        self.link=[_data objectForKey:@"link"];
        self.subCateId=[_data objectForKey:@"id"];
        self.subCateName =[_data objectForKey:@"name"];
        
    }
    return self;
}

@end
