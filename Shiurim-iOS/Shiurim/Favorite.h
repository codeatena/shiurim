//
//  Favorite.h
//  Shiurim
//
//  Created by Ahsan on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Song.h"

@interface Favorite : NSManagedObject

@property (nonatomic, retain) NSString * songId;
@property (nonatomic, retain) NSString * songName;
@property (nonatomic, retain) NSString * songURL;
@property (nonatomic, retain) NSString * catId;

+ (void ) addFavorite:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context;
+ (NSArray *) getFavoritesInManagedObjectContext:(NSManagedObjectContext *)_context;
+(BOOL) isAlreadyFavorite:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context;

@end
