//
//  BaseViewController.m
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark progress

- (void)showProgressForView:(UIView *)view WithMessage:(NSString *)message {
    progressView = view;
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    _hud.labelText = message;
}
- (void)hideProgressForView {
    if (_hud) {
        [MBProgressHUD hideHUDForView:progressView animated:YES];
        self.hud = nil;
    }
}
#pragma mark - ServiceDelegate
- (void) service:(BaseService*)service reponseRecieved:(id)object andStatusInfo:(NSDictionary *)statusInfo{
    
    [self hideProgressForView];
    
}
-(void)service:(BaseService *)service failedWithStatusInfo:(NSDictionary *)statusInfo{
    
    [self hideProgressForView];
    NSLog(@"Fail msg %@",statusInfo);
}

@end
