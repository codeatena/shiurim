//
//  BaseViewController.h
//  Shiurim
//
//  Created by Shani on 3/31/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseService.h"
#import "MBProgressHUD.h"

@interface BaseViewController : UIViewController<ServiceDelegate>{
    
    UIView *progressView;
}


@property (retain, nonatomic) MBProgressHUD *hud;

- (void)showProgressForView:(UIView *)view WithMessage:(NSString *)message;
- (void)hideProgressForView;

@end
