//
//  Download.m
//  Shiurim
//
//  Created by Ahsan on 4/7/13.
//  Copyright (c) 2013 White Space Conflict. All rights reserved.
//

#import "Download.h"
#import "Song.h"

@implementation Download

@dynamic catId;
@dynamic songId;
@dynamic songName;
@dynamic songURL;
@dynamic downloaded;

+ (void ) addDownload:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context{
    
    
    Download *download = (Download *)[NSEntityDescription insertNewObjectForEntityForName:@"Download" inManagedObjectContext:_context];
    [download setSongId:_song.songid];
    [download setSongName:_song.songName];
    [download setSongURL:_song.link];
    [download setCatId:_song.subCateId];
    [download setDownloaded:[NSNumber numberWithInt:1]];
    
}
+ (void) updateDownload:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context{

    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Download"
                                              inManagedObjectContext:_context];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"songId = %@ ", _song.songid];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setPredicate:predicate];
    [request setEntity:entity];
    NSError *error;
    
    NSArray *results = [_context executeFetchRequest:request error:&error] ;
    Download *download=(Download *)[results lastObject];
    [download setDownloaded:[NSNumber numberWithInt:2]];
    
}
+ (NSArray *) getDownloadsInManagedObjectContext:(NSManagedObjectContext *)_context{
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Download"
                                              inManagedObjectContext:_context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError *error;
    
    NSArray *results = [_context executeFetchRequest:request error:&error] ;
    
    return results;
    
}

+(BOOL) isAlreadyDownloaded:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context{

    BOOL exists=NO;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Download"
                                              inManagedObjectContext:_context];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"songId = %@ ", _song.songid];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setPredicate:predicate];
    [request setEntity:entity];
    NSError *error;
    
    NSArray *results = [_context executeFetchRequest:request error:&error] ;
   
    if([results count]>0)
        exists=YES;
    
    return exists;
}


+(int) songStatus:(Song *)_song inManagedObjectContext:(NSManagedObjectContext *)_context{
    
    BOOL exists=NO;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Download"
                                              inManagedObjectContext:_context];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"songId = %@ ", _song.songid];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setPredicate:predicate];
    [request setEntity:entity];
    NSError *error;
    
    NSArray *results = [_context executeFetchRequest:request error:&error] ;
    
    if([results count]>0){
        
        Download *dw = [results lastObject];
        
        return [dw.downloaded intValue];
    }
    return exists;
}

@end
